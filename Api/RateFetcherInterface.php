<?php

namespace SchumacherFM\M2T3Exchange\Api;


interface RateFetcherInterface
{

    /**
     * @param string $rateName
     *
     * @return float
     */
    public function getRate($rateName);
}
