<?php

namespace SchumacherFM\M2T3Exchange\Block;


class ExchangeRate extends \Magento\Framework\View\Element\Template
{
    /**
     * Captcha data
     *
     * @var \SchumacherFM\M2T3Exchange\Model\RateFetcher
     */
    protected $_rateFetcher = null;

    /**
     * @param \Magento\Framework\View\Element\Template\Context    $context
     * @param \SchumacherFM\M2T3Exchange\Model\RateFetcher $rateFetcher
     * @param array                                               $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \SchumacherFM\M2T3Exchange\Model\RateFetcher $rateFetcher,
        array $data = []
    ) {
        $this->_rateFetcher = $rateFetcher;
        parent::__construct($context, $data);
    }

    public function getRateEUR()
    {
        return $this->_rateFetcher->getRate('EUR');
    }
}
