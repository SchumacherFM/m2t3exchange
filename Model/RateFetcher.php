<?php

namespace SchumacherFM\M2T3Exchange\Model;

use Magento\Framework\HTTP\ClientInterface;
use Magento\Framework\Json\DecoderInterface;

class RateFetcher
{
    const BASE_URL = 'http://api.fixer.io/latest?base=';

    protected $_base = '';

    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    protected $_client = null;

    /**
     * @var \Magento\Framework\Json\DecoderInterface
     */
    protected $_jsonDecoder;

    /**
     * RateFetcher constructor.
     *
     * @param string          $baseName
     * @param ClientInterface $_client
     */
    public function __construct($baseName, ClientInterface $_client, DecoderInterface $decoder)
    {
        $this->_base = $baseName;
        $this->_client = $_client;
        $this->_jsonDecoder = $decoder;
    }

    /**
     * @param string $rateName
     *
     * @return float
     */
    public function getRate($rateName)
    {
        $this->_client->get($this->getURL());
        if ($this->_client->getStatus() !== 200) {
            return -2;
        }
        $json = $this->_client->getBody();
        $rates = (array)$this->_jsonDecoder->decode($json);

        if ($rates['base'] === $this->_base && isset($rates['rates'][$rateName])) {
            return (float)$rates['rates'][$rateName];
        }

        return -1;
    }

    /**
     * @return string
     */
    protected function getURL()
    {
        return self::BASE_URL . $this->_base;
    }
}
