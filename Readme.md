## Magento 2 Trained Partner Program: Developer Exercises 

# Unit Three. Layout

Add a block that shows the current exchange rate between the euro and the US dollar on every page. 
Use this URL for obtaining current rates: http://api.fixer.io/latest?base=USD (Training3_ExchangeRate)

